import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destino-viaje-state.model';
import { AppConfig, AppState, APP_CONFIG, db } from './../app.module';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.model';
import { forwardRef, Inject, Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient {

destinos: DestinoViaje[] = [];
current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) {
  this.store.select(state => state.destinos).subscribe((data) => {
   // console.log('destinos sub store');
   // console.log(data);
    this.destinos = data.items;
  });
  this.store.subscribe((data) => {
   // console.log('all store');
   // console.log(data);
  });
}

add(d: DestinoViaje){
  //this.store.dispatch(new NuevoDestinoAction(d));
  const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
  const req: any =  new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo: d.nombre}, {headers : headers});
  this.http.request(req).subscribe((data: HttpResponse<{}>) => {
    if (data.status === 200) {
      this.store.dispatch(new NuevoDestinoAction(d));
      const myDB = db;
      myDB.destinos.add(d);
      //console.log('Destinos en DB');
      myDB.destinos.toArray(); //.then(destinos => console.log(''));
    }
  });
}
//getAll(): DestinoViaje[]{
//return this.destinos;
//}

//getById(id: string) {
//  return this.destinos.filter(function(d){
//    return d.id.toString() === id;})[0];
//}
getById(id: string): DestinoViaje {
//  return this.store.dispatch(new )
  return this.destinos.filter((d) => { return d.id.toString() == id; })[0];
}
elegir(d: DestinoViaje) {
  //this.destinos.forEach(x => x.setSelected(false));
  this.store.dispatch(new ElegidoFavoritoAction(d));
  //d.setSelected(true);
  //this.current.next(d);
}

//subscribeOnChange(fn) {
//  this.current.subscribe(fn);
//}

}
