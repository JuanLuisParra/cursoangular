import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user: string, password: string) {
    if (user === 'user' && password === 'password'){
      console.log('logueado');
      localStorage.setItem('username', user);
      return true;
    }
    return false;
  }

  logout(): any {
    localStorage.removeItem('username');
  }

  getUser(): any {
    localStorage.getItem('username');
  }

  isLoggedIn(): any {
    return this.getUser() !== null;
  }
}
