import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})

export class EspiameDirective implements OnInit, OnDestroy {
  static nexiId = 0;
  log = (msg: string) => console.log('Evento #' + (++EspiameDirective.nexiId) + (msg));
  ngOnInit() { this.log('********* Init');}
  ngOnDestroy() { this.log('****** destroyed');}

  constructor() { }

}
