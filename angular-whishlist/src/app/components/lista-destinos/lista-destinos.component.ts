import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destino-viaje-state.model';
import { AppState } from './../../app.module';
import { Store } from '@ngrx/store';
import { DestinosApiClient } from './../../models/destino-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito).subscribe(d => {
        if (d != null){
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });

    store.select(state => state.destinos.items).subscribe(items => this.all = items);

    // this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
    //   if (d != null){
    //     this.updates.push('Se ha elegido a ' + d.nombre);
    //   }
    // });
  }

  ngOnInit(): void {
  }
  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViaje(nombre, url);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
   // this.store.dispatch(new NuevoDestinoAction(d));
  }
  elegido(e: DestinoViaje){
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true);
    this.destinosApiClient.elegir(e);
   // this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }

}
