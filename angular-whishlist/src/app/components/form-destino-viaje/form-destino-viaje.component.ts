import { DestinoViaje } from './../../models/destino-viaje.model';
import { Component, EventEmitter, forwardRef, inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG } from '../../app.module';
import { ResetAllAction, ResetVotesAction } from '../../models/destino-viaje-state.model';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  searchResults: string[];

  constructor(fb: FormBuilder, private store: Store<AppState>,
    @Inject( forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidador,
        this.nombreValidadorParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    });

    this.fg.valueChanges.subscribe((form: any) => {
     // console.log(' cambio en:' , form);
    });

    this.fg.controls['nombre'].valueChanges.subscribe(
      (value: string) => {
       // console.log('nombre cambió:', value);
      }
    );
  }

  ngOnInit(): void {
    const elemNombre = document.getElementById('nombre') as HTMLInputElement;
    fromEvent(elemNombre, 'input')
     .pipe (
       map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
       filter(text => text.length > 0 ),
       debounceTime(200),
       distinctUntilChanged(),
       switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text ))
       ).subscribe( ajaxResponse => this.searchResults = ajaxResponse.response);
//       switchMap(() => ajax('/assets/datos.json'))
//             //.filter( function(x){
        //return x.toLowerCase().includes(elemNombre.value.toLowerCase());
        //});
  }

  guardar(nombre: string, url: string): boolean{
    //console.log(nombre + ' - ' + url );
    const d = (new DestinoViaje(nombre, url));
    this.onItemAdded.emit(d);
    return false;
  }


  nombreValidador(control: FormControl): { [s: string]: boolean}{
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5){
      return {nombreInvalido: true};
    }
    return null;
    }

  nombreValidadorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): {[s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if (l>0 && l< minLong){
        return {minLongNombre: true};
      }
      return null;
    }
  }

  resetear(){
      this.store.dispatch(new ResetAllAction());
      return false;
    }
  }
